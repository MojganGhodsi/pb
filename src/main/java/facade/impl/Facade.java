package facade.impl;

import control.impl.Service;
import dto.*;
import model.entity.*;
import control.*;
import facade.interfaces.IFacade;

public class Facade implements IFacade<DTO>{
    private Entity contactEntity;
    private Service service=new Service();

    public  Facade(){

    }


    @Override
    public void mapContact(DTO contactDTO){
        NumberType nT=new NumberType();
        contactEntity = new Entity(contactDTO.getName(), nT.makeType(contactDTO.getType()),contactDTO.getNumber());
        service.add(contactEntity);

    }




}
