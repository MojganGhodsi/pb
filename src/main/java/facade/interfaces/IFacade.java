package facade.interfaces;

/**
 * Created by My PC on 10/24/2017.
 */
public interface IFacade<T> {
    public void mapContact(T t);
}
