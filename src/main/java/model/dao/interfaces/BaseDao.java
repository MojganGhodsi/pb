package model.dao.interfaces;


public interface BaseDao<T> {

    void add(T t);
}
