package model.dao.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;


public class ConnectionPool {


    private static ConnectionPool instance = null ;
    private static String dbURL = "jdbc:derby:demo;create=true";
    private static final int MAX_OF_CONNECTIONS = 3 ;
    private static final int MIN_OF_CONNECTIONS = 1 ;

    private ArrayList<Connection> freeConnections = new ArrayList<>();
    private ArrayList<Connection> usedConnections = new ArrayList<>();

    private ConnectionPool() {

        for (int i=0 ; i<MIN_OF_CONNECTIONS ; i++){
            addToPool();
        }
    }

    //singleton
    public static ConnectionPool getInstance() {
        if (instance == null){

            instance = new ConnectionPool();
        }

        return instance;
    }

    private void addToPool(){

        try {
            freeConnections.add(DriverManager.getConnection("jdbc:derby:demo;create=true"));
        } catch (SQLException e) {

            e.printStackTrace();
            System.out.println("core : couldn't get a derby connection");
        }
    }

    public void free(Connection cn){


        if (cn!= null && usedConnections.contains(cn)){

            freeConnections.add(cn);
            usedConnections.remove(cn);
        }
    }

    public Connection getConnection() {

        Connection cn ;

        if (usedConnections.size() <= MAX_OF_CONNECTIONS && freeConnections.size() ==0 ){
            addToPool();
        }

        if (!freeConnections.isEmpty()){

            cn = freeConnections.get(0);
            usedConnections.add(cn);
            freeConnections.remove(0);
            return cn;

        }
        else {

            System.out.println("Waiting ... ");
            return null;

        }
    }

}
