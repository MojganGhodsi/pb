package model.dao.impl;

import model.entity.Entity;
import model.dao.interfaces.BaseDao;
import model.dao.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;


public class ContactDao implements BaseDao<Entity> {

    ConnectionPool pool = ConnectionPool.getInstance();
    @Override
    public void add(Entity c) {
        try {
            Connection cn = pool.getConnection();
            String sqlQuery = "INSERT INTO CONTACTS (NAME, TYPE , NUMBER) VALUES (?, ? , ?)";
            PreparedStatement pstmt = cn.prepareStatement(sqlQuery);
            pstmt.setString(1, c.getName());
            pstmt.setString(2, c.getType());
            pstmt.setString(3 , c.getNumber());
            pstmt.executeUpdate();
            pstmt.close();



    } catch (SQLException sqlExcept) {

            if (sqlExcept.getMessage().equals("Table/View '" + "CONTACTS" + "' does not exist.")) {

                try {
                    Statement stmt = pool.getConnection().createStatement();
                    stmt.execute("CREATE TABLE " + "CONTACTS" +
                            "(ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," +
                            "NAME VARCHAR(255) , TYPE VARCHAR(255)  , NUMBER VARCHAR(255) , CONSTRAINT primary_key PRIMARY KEY (id))");
                    add(c);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}