package model.entity;

/**
 * Created by My PC on 10/20/2017.
 */
public class Entity {
    private String name;
    private String type;
    private String number;

    public Entity(){
    }

    public Entity(String name,String type ,String number){
        this.name=name;
        this.type=type;
        this.number=number;
    }

    public void setName(String name){
        this.name=name;
    }
    public void setType(String type){

        this.type= type;
    }
    public void setNumber(String number){
        this.number=number;
    }
    public String getName(){
        return name;
    }
    public String getType(){
        return type;
    }
    public String getNumber(){
        return number;
    }
//    public String getContact(String number){
//        return name + "\t " + type + "\t " + number.toString() ;
//    }
}
