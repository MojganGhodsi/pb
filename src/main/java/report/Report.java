//package report;
//
//import log.LogHandler;
//import model.entity.Entity;
//import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
//import net.sf.dynamicreports.report.builder.DynamicReports;
//import net.sf.dynamicreports.report.builder.component.Components;
//import net.sf.dynamicreports.report.builder.style.StyleBuilder;
//import net.sf.dynamicreports.report.constant.HorizontalAlignment;
//import net.sf.dynamicreports.report.constant.VerticalAlignment;
//
//import javax.imageio.ImageIO;
//import java.awt.image.BufferedImage;
//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//
//import static net.sf.dynamicreports.report.builder.DynamicReports.cmp;
//import static net.sf.dynamicreports.report.builder.DynamicReports.stl;
//
//public abstract class Report{
//
//    private JasperReportBuilder report = null;
//
//    public Report() {
//        report = DynamicReports.report();
//    }
//
//    public void generate(ArrayList<Entity> contacts ) {
//
//        new Thread(() -> {
////            try {
//                setReportsDesign();
//                setColumns();
//                setData(contacts);
//                save();
//            } catch (Exception e) {
//                LogHandler.getInstance().setWarningLog(e.toString());
//            }
//        }).
//
//    start() {
//
//    }
//
//}
//
//
//    private void setReportsDesign(){
//
//        StyleBuilder boldStyle = stl.style().bold();
//
//        StyleBuilder boldCenteredStyle = stl.style(boldStyle)
//                .setHorizontalAlignment(HorizontalAlignment.CENTER);
//
//        StyleBuilder titleStyle = stl.style(boldCenteredStyle)
//                .setVerticalAlignment(VerticalAlignment.MIDDLE)
//                .setFontSize(15);
//
//        BufferedImage img ;
//        try {
//
//            ClassLoader classLoader = getClass().getClassLoader();
//            img = ImageIO.read(new File(classLoader.getResource("header.png").getFile()));
//
//        } catch (IOException e) {
//            LogHandler.getInstance().setWarningLog(e.toString());
//        }
//
//        report.highlightDetailEvenRows()
//                .title(cmp.horizontalList().add(
//                        cmp.image(img).setFixedDimension(80, 80) ,
//                        cmp.text("DynamicReports").setStyle(titleStyle).setHorizontalAlignment(HorizontalAlignment.LEFT))
//                        .newRow()
//                        .add(cmp.filler().setStyle(stl.style().setTopBorder(stl.pen2Point())).setFixedHeight(10)))
//                .pageFooter(Components.pageXofY().setStyle(boldCenteredStyle));
//        //JRProperties.setProperty(QueryExecuterFactory.QUERY_EXECUTER_FACTORY_PREFIX+"plsql" ,"com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");
//    }
//
//    public abstract void setColumns();
//
//    public abstract void setData(ArrayList<Entity> contacts);
//
//    public abstract void save();
//
//    JasperReportBuilder getReport() {
//        return report;
//    }
//
//
//}
