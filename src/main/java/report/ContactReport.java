//package report;
//
//
//import log.LogHandler;
//import model.entity.Entity;
//import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
//import net.sf.dynamicreports.report.builder.datatype.DataTypes;
//import net.sf.dynamicreports.report.builder.style.StyleBuilder;
//import net.sf.dynamicreports.report.constant.HorizontalAlignment;
//import net.sf.dynamicreports.report.datasource.DRDataSource;
//import net.sf.dynamicreports.report.exception.DRException;
//
//import java.awt.*;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.util.*;
//
//import static net.sf.dynamicreports.report.builder.DynamicReports.col;
//import static net.sf.dynamicreports.report.builder.DynamicReports.stl;
//
//public class ContactReport extends Report {
//
//    private DRDataSource dataSource = new DRDataSource("ID", "NAME" , "PHONE_BOOK_ID" , "NUMBER" , "TYPE");
//
//    public ContactReport() {
//        super();
//    }
//
//
//    public void setColumns() {
//
//        StyleBuilder boldStyle = stl.style().bold();
//        StyleBuilder boldCenteredStyle = stl.style(boldStyle)
//                .setHorizontalAlignment(HorizontalAlignment.CENTER);
//        StyleBuilder columnTitleStyle  = stl.style(boldCenteredStyle)
//                .setBorder(stl.pen1Point())
//                .setBackgroundColor(Color.LIGHT_GRAY);
//
//
//        TextColumnBuilder<Integer> idColumn  = col.column("ID", "ID",  DataTypes.integerType())
//                .setFixedColumns(5)
//                .setHorizontalAlignment(HorizontalAlignment.CENTER);
//
//        TextColumnBuilder<String>  nameColumn  = col.column("Name" , "NAME" , DataTypes.stringType()).setStyle(boldStyle)
//                .setHorizontalAlignment(HorizontalAlignment.CENTER);
//
//        TextColumnBuilder<String>  TypeNumberColumn = col.column("Number type" , "PHONE_BOOK_ID" , DataTypes.stringType())
//                .setHorizontalAlignment(HorizontalAlignment.CENTER);
//
//        TextColumnBuilder<String>  numberColumn  = col.column("Number" , "NUMBER" , DataTypes.stringType())
//                .setHorizontalAlignment(HorizontalAlignment.CENTER);
//
//
//
//        this.getReport().setColumnTitleStyle(columnTitleStyle)
//
//
//                .columns(
//
//                        idColumn ,
//                        nameColumn ,
//                        TypeNumberColumn ,
//                        numberColumn )
//
//                .setColumnTitleStyle(columnTitleStyle);
//
//        //show page number on the page footer
//
//    }
//
//    @Override
//    public void setData(ArrayList<Entity> contacts) {
//
//        Collections.sort(contacts, (o1, o2) -> o1.getName().compareTo(o2.getName()));
//
//        for (Entity c : contacts){
//
//            setContactData(c);
//
//        }
//
//        this.getReport().setDataSource(dataSource);
//    }
//
//    @Override
////    public void save(){
////
////        //export the report as a pdf file
////        try {
////
////            ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:/annotation/config.xml");
////            PropertyHandler property = (PropertyHandler) ctx.getBean("Property");
////            property.loadProperties();
////
////            this.getReport().show();
////            this.getReport().toPdf(new FileOutputStream(property.getProperties("ContactsPdfStoragePath")));
////            this.getReport().toXlsx(new FileOutputStream(property.getProperties("ContactsExcelStoragePath")));
////
////        } catch (DRException e) {
////            LogHandler.getInstance().setWarningLog(e.toString());
////
////        } catch (FileNotFoundException e) {
////            LogHandler.getInstance().setWarningLog(e.toString());
////        };
////
////    }
//
//
//    private void setContactData(Entity contact){
//        int i=0;
//        for (Entity c : contact){
//            i++;
//            dataSource.add( i , contact.getName() ,  contact.getType() , contact.getNumber());
//
//
//        }
//    }
//}
