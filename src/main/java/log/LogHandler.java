package log;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LogHandler {

    private static LogHandler instance ;
    private static final Logger logger = Logger.getLogger("PhoneBookLog");
    private FileHandler fileHandler ;

    private LogHandler() {

        try {
            fileHandler = new FileHandler("log.txt" , true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.addHandler(fileHandler);
        logger.setLevel(Level.WARNING);
    }

    public static LogHandler getInstance(){

        if (instance == null){
            return new LogHandler();
        }
        return instance;

    }

    public  void setInfoLog(String msg){

        logger.info(msg);


    }

    public void setWarningLog(String msg){

        logger.warning(msg);
    }


}
