package dto;

public class DTO {
    private String name;
    private int type=0;
    private String number;


    public DTO(){
    }

    public DTO(String name,int type ,String number){
        this.name=name;
        this.type=type;
        this.number=number;
    }

    public void setName(String name){
        this.name=name;
    }
    public void setType(int type)
    {
        this.type=type;
    }
    public void setNumber(String number){
        this.number=number;
    }
    public String getName(){
        return name;
    }
    public int getType(){
        return type;
    }
    public String getNumber(){
        return number;
    }
//    public String getContact(String number){
//        return name + "\t " + type + "\t " + number.toString() ;
//    }
}